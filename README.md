# README #

Prototype of game inspired by LoZ. Written to build a better understanding of game design process, documentation, and general practices for collisions & object interaction.

### Media ###

* Code & sprites produced exclusively by Evan Bunderle ("poodragon") except for draw_text_bordered_opt.gml originally provided by [wolfreak99](https://github.com/wolfreak99)
* Music created utilizing [Wolfram Tones](http://tones.wolfram.com)

### Licensing & Source ###

Source is free to use as originally provided herein, except for content provided by wolfreak99. Please contact original contributor for rights to use scripts either at [GitHub](https://github.com/wolfreak99) or [Reddit](http://www.reddit.com/user/wolfreak_99).