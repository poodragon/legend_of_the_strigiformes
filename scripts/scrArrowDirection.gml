// Returns value which determines the direction the arrow is to fly

direc = 270;

if keyboard_check_pressed(vk_right) {
    direc = 0;
} else if keyboard_check_pressed(vk_up) {
    direc = 90;
} else if keyboard_check_pressed(vk_left) {
    direc = 180;
} else if keyboard_check_pressed(vk_down) {
    direc = 270;
}

return direc;
