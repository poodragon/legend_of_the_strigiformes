//Check if left or right direction is being held, returns digit 1, 0, -1

return keyboard_check(ord('D')) - keyboard_check(ord('A'));